#!/bin/bash

# This script is part of the convenience build system for the IWR Dune course.
# It installs all external dependencies of Dune used throughout the course.

# There are no external dependencies at the moment, yay!
